import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Observable} from 'rxjs/Observable';
import {CrudProvider} from "../../providers/crud/crud";
import {ViewLanguagePage} from "../view-language/view-language";

@Component({
   selector: 'page-list',
   templateUrl: 'list.html'
})
export class ListPage {

   private _languages: Observable<any[]>;



   constructor(private crudProvider: CrudProvider, private navController: NavController) {
      this.languages = this.crudProvider.read('languages');
   }



   openLanguage(language: object) {
      this.navController.push(ViewLanguagePage, {language: language});
   }



   get languages(): Observable<any[]> {
      return this._languages;
   }

   set languages(value: Observable<any[]>) {
      this._languages = value;
   }
}
