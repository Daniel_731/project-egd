import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewLanguagePage } from './view-language';

@NgModule({
  declarations: [
    ViewLanguagePage,
  ],
  imports: [
    IonicPageModule.forChild(ViewLanguagePage),
  ],
})
export class ViewLanguagePageModule {}
