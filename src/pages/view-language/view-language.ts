import {Component} from '@angular/core';
import {IonicPage, NavParams} from 'ionic-angular';

@IonicPage()
@Component({
   selector: 'page-view-language',
   templateUrl: 'view-language.html',
})
export class ViewLanguagePage {

   private _language: object;



   constructor(private navParams: NavParams) {
      this.language = this.navParams.get('language');
   }



   get language(): object {
      return this._language;
   }

   set language(value: object) {
      this._language = value;
   }
}
