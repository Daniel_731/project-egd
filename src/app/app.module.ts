import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {ViewLanguagePage} from "../pages/view-language/view-language";

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { CrudProvider } from '../providers/crud/crud';


export const firebaseConfig = {
   apiKey: "AIzaSyBXGly7H35yd3Bk9oqSl8cENxn-50xXVCQ",
   authDomain: "project-egd.firebaseapp.com",
   databaseURL: "https://project-egd.firebaseio.com",
   projectId: "project-egd",
   storageBucket: "",
   messagingSenderId: "261987583708",
   appId: "1:261987583708:web:9bfa63506e79276f"
};


@NgModule({
   declarations: [
      MyApp,
      HomePage,
      ListPage,
      ViewLanguagePage,
   ],

   imports: [
      BrowserModule,
      IonicModule.forRoot(MyApp),
      AngularFireModule.initializeApp(firebaseConfig),
      AngularFireDatabaseModule,
      AngularFireAuthModule,
   ],

   bootstrap: [
      IonicApp
   ],

   entryComponents: [
      MyApp,
      HomePage,
      ListPage,
      ViewLanguagePage,
   ],

   providers: [
      StatusBar,
      SplashScreen,
      AngularFireDatabase,
      CrudProvider,
      {
         provide  : ErrorHandler,
         useClass : IonicErrorHandler
      },
   ]
})
export class AppModule {
}
