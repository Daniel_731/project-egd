import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';


@Injectable()
export class CrudProvider {

   constructor(private angularFireDatabase: AngularFireDatabase) {}



   public create(table: string, item: object) {
      if (!item['id']) {
         item['id'] = Date.now();
      }

      return this.angularFireDatabase.database.ref(`/${table}/${item['id']}`).set(item);
   }

   public read(table: string) {
      return this.angularFireDatabase.list(table).valueChanges();
   }

   public readById(table: string, id: any) {
      return this.angularFireDatabase.object(`/${table}/${id}`).valueChanges();
   }

   public update(table: string, item: object) {
      // @ts-ignore
      return this.angularFireDatabase.database.ref(`/${table}/${item['id']}`).set(item);
   }

   public delete(table: string, id: any) {
      return this.angularFireDatabase.object(`/${table}/${id}`).remove();
   }

}
